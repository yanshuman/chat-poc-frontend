import React from 'react' ;
import Firebase from 'firebase' ;
import PropTypes from 'prop-types' ;

class ChatBox extends React.Component {
  constructor (props) {
    super(props) ;
    this.state = {
      inputValue: '',
      messages: {}
    } ;
    this.propTypes = {
      chatRoomId: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      active: PropTypes.bool
    }
  }

  /**********************************
   * Life Cycle Hooks
   **********************************/
  componentDidMount = () => {
    let config = {
      apiKey: "AIzaSyDjMt2VY9jWug5IIsQaMNjWXqyv2hOOT3E",
      authDomain: "tekagogo-chat-box.firebaseapp.com",
      databaseUrl: "https://tekagogo-chat-box.firebaseio.com",
      storageBucket: "tekagogo-chat-box.appspot.com",
    };

    firebase.initializeApp(config);
    console.log("Firebase initialized.") ;

    let email = "anshuman.saagar@outlook.com";
    let password = "password" ;
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(user => {
      console.log("Signed as user: ", user) ;
    })
    .catch(function(error) {
      let errorCode = error.code;
      let errorMessage = error.message;
      console.log("Error authenticating with FB", errorCode, errorMessage) ;
    });
    // firebase.auth().signOut().then(() => {}).catch(() => {}) ;
    // Get hold of the database reference
    let db = firebase.database() ;

    db.ref('/threads/-LRbk8jpQZgDd4BRw0Zw/messages').on('value', snapshot => {
      console.log("snapshot", snapshot.val()) ;
      this.setState({messages: snapshot.val()}) ;
    }) ;
  }

  /**********************************
   * Evnet Handlers
   **********************************/
  handleSend = event => {
    console.log("Enter pressed");
    if(event.key === "Enter") {
      this.sendMessage() ;
    }
  }

  sendMessage = () => {
    console.log("inside sendMessage") ;
    firebase.database().ref('/threads/-LRbk8jpQZgDd4BRw0Zw/messages').push(this.state.inputValue)
    .then((args) => {
      console.log("Executed", args);
      this.setState({
        inputValue: ''
      })
    })
    .catch( (error) => {
      console.log("error", error) ;
    }) ;
  }

  setInputValue = (event) => {
    this.setState({
      inputValue: event.target.value
    })
  }

  /**********************************
   * Render
   **********************************/
  render() {

    let messages = { ...this.state.messages } ;
    let chatMessages = Object.keys(messages)
      .map(message => <p key={message}>{messages[message].content}</p>) ;

    return (
      <div className="App">
        <header className="App-header">
          <div>{chatMessages}</div>
          <div id="chat-combo">
            <input id="chat-input" value={this.state.inputValue} onChange={this.setInputValue} onKeyPress={this.handleSend}/>
            <button id="chat-send" onClick={this.sendMessage}>Send</button>
          </div>
        </header>
      </div>
    );
  }
}

export default ChatBox ;
